# EmoNet: Changelog

## Version 1
### 1.0
#### 1.0.7
Added missing dependency to ```pyproject.toml``` and ```requirements.txt```.

#### 1.0.6
Fixed broken URL(s) to data files.

#### 1.0.5
Removed "Repository" link, as it was the same as the "Homepage", except for an additional ```.git``` that didn't help anyone. All this in the hope that libraries.io will now finally detect the repository and ```README.md``` file.

#### 1.0.4
Fixed a faulty repository link.

#### 1.0.3
When the data is downloaded in case of a package install through pip, the downloaded files are now added to the ```RECORD``` metadata file, so that these files are also automatically removed when uninstalling the package.

#### 1.0.2
Moved the ```data``` folder to ```emonet_py/data```. The hope was that this way the data, when downloaded in case of a pip install, would also automatically be deleted upon uninstalling the package, which is not the case.

#### 1.0.1
Added script to, in case of a package install, download the data files from the GitLab repository using the GitLab API. 

#### 1.0.0
Initial release. This release attempted to include the 'data' folder in the distribution, which failed because the data is too big.